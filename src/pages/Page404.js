import {Container} from "react-bootstrap"

export default function Page404(){
    return <Container className="d-flex justify-content-center"><h1>404</h1></Container>
}